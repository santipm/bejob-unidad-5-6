<?php


function login_redirect(){
  global $pagenow;
  if( 'wp-login.php' == $pagenow ) {
    if ( isset( $_POST['wp-submit'] ) ||   // in case of LOGIN
      ( isset($_GET['action']) && $_GET['action']=='logout') ||   // in case of LOGOUT
      ( isset($_GET['checkemail']) && $_GET['checkemail']=='confirm') ||   // in case of LOST PASSWORD
      ( isset($_GET['checkemail']) && $_GET['checkemail']=='registered') ) return;    // in case of REGISTER
    else wp_redirect( home_url('/login')); 
    exit();
  }
} 
add_action('init','login_redirect');


function page_login_func(){

	if(get_the_id() == 9 || get_the_id() == 17 || get_the_id() == 34){
    wp_dequeue_style('smartlib_bootstrap' );

    wp_enqueue_style( 'bootstrap-style', get_stylesheet_directory_uri(). '/css/bootstrap.min.css');
    wp_enqueue_style( 'bootstrap-responsive', get_stylesheet_directory_uri(). '/css/bootstrap-responsive.min.css');
    wp_enqueue_style( 'style-child', get_stylesheet_directory_uri(). '/css/style.css');
    wp_enqueue_style( 'style-responsive', get_stylesheet_directory_uri(). '/css/style-responsive.css');

     wp_enqueue_style( 'fonts-google', 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext');
   
	}

}

add_action('wp_enqueue_scripts', 'page_login_func',99,1 );


function hide_adminbar(){
  if(!current_user_can('manage_options' )){ 
      show_admin_bar(false);
      echo '<style type="text/css">
      body { margin-top: -32px !important; }
      </style>';
    }
}

add_action('wp_head','hide_adminbar');
